# Contains the models for the author

from django.db import models
from django.contrib.auth.models import AbstractBaseUser, BaseUserManager

# Custom User Manager for Author
class AuthorManager(BaseUserManager):
    # Creating normal user
    def create_user(self, username, name, password=None):
        if not username:
            raise ValueError("Authors must have a username")
        if not name:
            raise ValueError("Authors must have a name")

        user = self.model(
            username=username,
            name=name,
        )

        user.set_password(password)
        user.save(using=self._db)
        return user

    def create_superuser(self, username, name, password=None):
        if not username:
            raise ValueError("Authors must have a username")
        if not name:
            raise ValueError("Authors must have a name")

        user = self.model(
            username=username,
            name=name,
        )

        user.set_password(password)
        user.is_admin = True
        user.save(using=self._db)
        return user

# Author Model built from AbstractBaseUser
class Author(AbstractBaseUser):
    username = models.CharField(max_length=30, unique=True)
    name = models.CharField(max_length=100)
    is_admin = models.BooleanField(default=False)

    objects = AuthorManager()

    # set a required field
    USERNAME_FIELD = 'username'

    def has_perm(self, perm, obj=None):
        return True

    def has_module_perms(self, app_label):
        return True

    @property
    def is_staff(self):
        return self.is_admin

# Story Model
class Story(models.Model):
    # Choices of categories
    CATEGORY_CHOICES = [
        ('pol', 'Politics'),
        ('art', 'Art'),
        ('tech', 'Technology'),
        ('trivia', 'Trivia'),
    ]

    # Choices of regions
    REGION_CHOICES = [
        ('uk', 'UK'),
        ('eu', 'Europe'),
        ('w', 'World'),
    ]

    # Limits on fields
    headline = models.CharField(max_length=64)
    category = models.CharField(max_length=10, choices=CATEGORY_CHOICES)
    region = models.CharField(max_length=10, choices=REGION_CHOICES)
    author = models.ForeignKey(Author, on_delete=models.CASCADE)
    date = models.DateField(auto_now_add=True)
    details = models.CharField(max_length=128)
