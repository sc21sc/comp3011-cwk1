from django.urls import path
from .api import LoginView, LogoutView, StoriesView, StoryDetailView

# URLs for my API
urlpatterns = [
    path('login', LoginView.as_view(), name='login'),
    path('logout', LogoutView.as_view(), name='logout'),
    path('stories', StoriesView.as_view(), name='stories'),
    path('stories/<int:pk>', StoryDetailView.as_view(), name='story_detail'),
]