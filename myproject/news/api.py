from django.contrib.auth import authenticate, login
from django.contrib.auth import logout

from rest_framework import status
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status

from .models import Story
from .serializers import StorySerializer, NewsSerializer



# Login
class LoginView(APIView):
    def post(self, request, format=None):
        username = request.data.get('username')
        password = request.data.get('password')
        user = authenticate(request, username=username, password=password)
        if user is not None:
            login(request, user)
            return Response({"message": "Login successful"}, status=status.HTTP_200_OK)
        else:
            return Response({"message": "Invalid credentials"}, status=status.HTTP_400_BAD_REQUEST)

# Logout
class LogoutView(APIView):
    def get(self, request, format=None):
        try:
            logout(request)
            return Response({"message": "Logged out successfully"}, status=status.HTTP_200_OK)
        except:
            return Response({"message":"Error loggin out"},status=status.HTTP_400_BAD_REQUEST)

    


class StoriesView(APIView):
    def get(self, request, format=None):
        # Extract potential filter parameters from the request
        category = request.query_params.get('story_cat')
        region = request.query_params.get('story_region')
        date = request.query_params.get('story_date')

        # Start with all stories
        stories = Story.objects.all()

        # Apply filters if provided
        if category:
            stories = stories.filter(category=category)
        if region:
            stories = stories.filter(region=region)
        if date:
            stories = stories.filter(date=date)

        if stories.count() == 0:
            return Response({"message": "No stories were found"}, status=status.HTTP_404_NOT_FOUND)
        
        # Serialize and return the filtered set of stories
        serializer = NewsSerializer(stories, many=True)

        return Response({"stories": serializer.data})

    def post(self, request, format=None):
        if not request.user.is_authenticated:           
            return Response({"message": "Authentication required"}, status=status.HTTP_503_SERVICE_UNAVAILABLE)
        
        # If the serialization isn't valid return 503

        serializer = StorySerializer(data=request.data)
        if not serializer.is_valid():
            return Response({"message:": "There was an error creating the post"}, status=status.HTTP_503_SERVICE_UNAVAILABLE)
        
        serializer.save(author=request.user)
        return Response(serializer.data, status=status.HTTP_201_CREATED)


class StoryDetailView(APIView):
    def delete(self, request, pk, format=None):
        if not request.user.is_authenticated:
            return Response({"message": "Authentication required"}, status=status.HTTP_503_SERVICE_UNAVAILABLE)
        
        # The user can only delete their own story, admin can delete anyones
        try:
            story = Story.objects.get(pk=pk)
            if story.author != request.user and not request.user.is_staff:
                return Response({"message": "You do not have permission to delete this story"}, status=status.HTTP_503_SERVICE_UNAVAILABLE)
            story.delete()
            return Response(status=status.HTTP_200_OK)
        except Story.DoesNotExist:
            return Response({"message": "Error deleting the story, it may not exist"}, status=status.HTTP_503_SERVICE_UNAVAILABLE)