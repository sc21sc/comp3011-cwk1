# Serializes the data for input and output

from rest_framework import serializers
from .models import Story

class StorySerializer(serializers.ModelSerializer):
    author = serializers.ReadOnlyField(source='author.id')
    
    # data for each story
    class Meta:
        model = Story
        fields = ['id', 'headline', 'category', 'region', 'author', 'date', 'details']

class NewsSerializer(serializers.ModelSerializer):
    author = serializers.CharField(source='author.name')
    key = serializers.SerializerMethodField()

    story_cat = serializers.CharField(source='category')
    story_region = serializers.CharField(source='region')
    story_date = serializers.CharField(source='date')
    story_details = serializers.CharField(source='details')

    class Meta:
        model = Story
        # fields = ['id', 'headline', 'category', 'region', 'author', 'date', 'details']
        fields = ['key','headline','story_cat','story_region', 'author','story_date','story_details']

    def get_key(self, obj):
        return str(obj.id)