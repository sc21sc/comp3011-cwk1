from django.contrib import admin
from django.urls import path, include

# URLs for api defined in news.urls
urlpatterns = [
    path('admin/', admin.site.urls),
    path('api/', include('news.urls')),
]
